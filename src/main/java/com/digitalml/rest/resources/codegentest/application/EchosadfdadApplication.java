package com.digitalml.rest.resources.codegentest.application;

import com.digitalml.rest.resources.codegentest.resource.EchosadfdadResource;
import com.digitalml.rest.resources.codegentest.resource.ResourceLoggingFilter;					
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Configuration;

import java.util.EnumSet;
import javax.servlet.DispatcherType;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.joran.spi.JoranException;


public class EchosadfdadApplication extends Application<EchosadfdadConfiguration> {

	public static void main(final String[] args) throws Exception {
		new EchosadfdadApplication().run(args);
	}
	
	@Override
	public void initialize(final Bootstrap<EchosadfdadConfiguration> bootstrap) {
		// TODO: application initialization
	}

	@Override
	public void run(final EchosadfdadConfiguration configuration, final Environment environment) {

		//separate logging setup to enable use of external logback xml
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		context.reset();
		ContextInitializer initializer = new ContextInitializer(context);
		try {
			initializer.autoConfig();
		} catch (JoranException e) {
		}

		final EchosadfdadResource resource = new EchosadfdadResource();
		final EchosadfdadHealthCheck healthCheck = new EchosadfdadHealthCheck();
		
		environment.servlets().addFilter("ResourceLoggingFilter", new ResourceLoggingFilter()).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
		
		environment.healthChecks().register("template", healthCheck);
		environment.jersey().register(resource);

	}
}

final class EchosadfdadConfiguration extends Configuration {
}

final class EchosadfdadHealthCheck extends HealthCheck {

	public EchosadfdadHealthCheck() {
	}

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}
}